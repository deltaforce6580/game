package net.mineclick.game.model;

import lombok.Data;
import net.mineclick.game.service.*;
import net.mineclick.game.type.Rarity;
import net.mineclick.game.type.skills.SkillType;
import net.mineclick.global.type.Rank;
import net.mineclick.global.util.Formatter;
import net.mineclick.global.util.location.RandomVector;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

@Data
public class DailyRewardChest {
    private static final int MAX_CLICKS = 15;
    private static final int EXP = 2;
    private static final int SCHMEPLS = 20;

    private Instant refreshAt = Instant.now();
    private int clicks = 0;

    public boolean isEmpty() {
        return clicks >= MAX_CLICKS;
    }

    public boolean isRefreshed() {
        return refreshAt.isBefore(Instant.now());
    }

    public void handleClick(GamePlayer player) {
        if (isEmpty() || !isRefreshed()) {
            return;
        }

        clicks++;
        if (isEmpty()) {
            refreshAt = Instant.now().plus(23, ChronoUnit.HOURS);
        }

        Location loc = LobbyService.i().getRewardsChest();
        if (player.isRankAtLeast(Rank.PAID) && clicks <= 5) {
            Rarity rarity = GeodesService.i().addGeode(player);
            spawnHologram(1, rarity.getGeodeName() + " geode", player);
        }

        //Give rewards
        int multiplier = SkillsService.i().has(player, SkillType.MISC_3) ? 2 : 1;
        double levelMultiplier = getLevelMultiplier(player);
        player.addExp((int) (EXP * multiplier * levelMultiplier));
        player.addSchmepls((int) (SCHMEPLS * multiplier * levelMultiplier));

        spawnHologram((int) (EXP * multiplier * levelMultiplier), "EXP", player);
        spawnHologram((int) (SCHMEPLS * multiplier * levelMultiplier), "schmepls", player);
    }

    private double getLevelMultiplier(GamePlayer player) {
        int level = LevelsService.i().getLevel(player.getExp());
        return 1 + (level - 1) * 0.05; // This will give a multiplier of 1 for level 1, 1.05 for level 2, and so on.
    }

    private void spawnHologram(int value, String text, GamePlayer player) {
        Location loc = LobbyService.i().getRewardsChest();
        HologramsService.i().spawnFloatingUp(loc.clone().add(new RandomVector()), p -> ChatColor.GREEN + "+" + ChatColor.YELLOW + value + ChatColor.AQUA + " " + text, Collections.singleton(player));
    }

    public String getTimeLeft() {
        long diff = refreshAt.toEpochMilli() - System.currentTimeMillis();

        if (diff > 0) {
            return Formatter.duration(diff);
        } else {
            return "00:00:00";
        }
    }
}
